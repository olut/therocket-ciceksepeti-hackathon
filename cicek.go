package main

import (
	"bufio"
	"container/heap"
	"encoding/csv"
	"fmt"
	"image/color"
	"io"
	"log"
	"math"
	"os"
	"strconv"

	"github.com/fogleman/gg"
	"github.com/golang/geo/s2"

	sm "github.com/flopp/go-staticmaps"
)

type Point struct {
	Lat float64
	Lon float64
}

type delivery struct {
	Id         int
	Coordinate Point
}

type Store struct {
	Color      string
	Coordinate Point
	MaxCap     int
	MinCap     int
	Current    int
}

type Node struct {
	Id         int
	Distance   float64
	Coordinate Point
}

func distance(p1 Point, p2 Point) float64 {
	const PI float64 = 3.141592653589793

	radlat1 := float64(PI * p1.Lat / 180)
	radlat2 := float64(PI * p2.Lat / 180)

	theta := float64(p1.Lon - p2.Lon)
	radtheta := float64(PI * theta / 180)

	dist := math.Sin(radlat1)*math.Sin(radlat2) + math.Cos(radlat1)*math.Cos(radlat2)*math.Cos(radtheta)

	if dist > 1 {
		dist = 1
	}

	dist = math.Acos(dist)
	dist = dist * 180 / PI
	dist = dist * 60 * 1.1515
	dist = dist * 1.609344

	return dist
}

type DistanceHeap []Node

func (h DistanceHeap) Len() int           { return len(h) }
func (h DistanceHeap) Less(i, j int) bool { return h[i].Distance < h[j].Distance }
func (h DistanceHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *DistanceHeap) Push(x interface{}) {
	item := x.(Node)
	*h = append(*h, item)
}
func (h *DistanceHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

func create_heap(deliveries []delivery, p Point) DistanceHeap {
	result := make(DistanceHeap, len(deliveries))
	for i, delivery := range deliveries {
		result[i] = Node{
			Id:         delivery.Id,
			Distance:   distance(p, delivery.Coordinate),
			Coordinate: delivery.Coordinate,
		}
	}
	heap.Init(&result)
	return result
}

func main() {
	deliveryFile, _ := os.Open("siparis.csv")
	storeFile, _ := os.Open("bayi.csv")

	visited := make(map[int]bool)
	// IO begins
	reader := csv.NewReader(bufio.NewReader(deliveryFile))

	var deliveries []delivery
	for {
		line, error := reader.Read()
		if error == io.EOF {
			break
		} else if error != nil {
			log.Fatal(error)
		}
		id, err := strconv.Atoi(line[0])
		if err != nil {
			continue
		}
		visited[id] = false
		lat, _ := strconv.ParseFloat(line[1], 32)
		lon, _ := strconv.ParseFloat(line[2], 32)
		deliveries = append(deliveries, delivery{
			Id: id,
			Coordinate: Point{
				Lat: lat,
				Lon: lon,
			},
		})
	}
	reader = csv.NewReader(bufio.NewReader(storeFile))
	var stores []Store
	for {
		line, error := reader.Read()
		if error == io.EOF {
			break
		} else if error != nil {
			log.Fatal(error)
		}

		lat, err := strconv.ParseFloat(line[1], 32)
		if err != nil {
			continue
		}
		lon, _ := strconv.ParseFloat(line[2], 32)

		stores = append(stores, Store{
			Color: line[0],
			Coordinate: Point{
				Lat: lat,
				Lon: lon,
			},
		})
	}

	// IO ends

	// Logic
	var redStore = stores[0]
	var greenStore = stores[1]
	var blueStore = stores[2]

	// Capacities are given hard coded since they are not provided in CSV file.
	redStore.MaxCap, redStore.MinCap = 30, 20
	greenStore.MaxCap, greenStore.MinCap = 50, 35
	blueStore.MaxCap, blueStore.MinCap = 80, 20

	// Create heaps take minimum distance
	redHeap := create_heap(deliveries, redStore.Coordinate)
	blueHeap := create_heap(deliveries, blueStore.Coordinate)
	greenHeap := create_heap(deliveries, greenStore.Coordinate)

	totalCost := float64(0)

	var redResult, greenResult, blueResult []Node

	var counter = 0
	for counter < len(deliveries) {
		var redNearest, blueNearest, greenNearest Node
		redNearest.Distance = 1500000000
		greenNearest.Distance = 1500000000
		blueNearest.Distance = 1500000000

		if len(blueHeap) > 0 {
			blueNearest = heap.Pop(&blueHeap).(Node)
		}
		if len(greenHeap) > 0 {
			greenNearest = heap.Pop(&greenHeap).(Node)
		}
		if len(redHeap) > 0 {
			redNearest = heap.Pop(&redHeap).(Node)
		}

		for visited[redNearest.Id] != false && len(redHeap) > 0 {
			redNearest = heap.Pop(&redHeap).(Node)
		}
		for visited[blueNearest.Id] != false && len(blueHeap) > 0 {
			blueNearest = heap.Pop(&blueHeap).(Node)
		}
		for visited[greenNearest.Id] != false && len(greenHeap) > 0 {
			greenNearest = heap.Pop(&greenHeap).(Node)
		}

		if redNearest.Distance <= blueNearest.Distance && redNearest.Distance <= greenNearest.Distance {
			if visited[redNearest.Id] == false && redStore.Current < redStore.MaxCap {
				redStore.Current++
				totalCost += redNearest.Distance
				visited[redNearest.Id] = true
				redResult = append(redResult, redNearest)
				heap.Push(&greenHeap, greenNearest)
				heap.Push(&blueHeap, blueNearest)
				counter++
			} else {
				heap.Push(&greenHeap, greenNearest)
				heap.Push(&blueHeap, blueNearest)
			}
		} else if blueNearest.Distance <= redNearest.Distance && blueNearest.Distance <= greenNearest.Distance {
			if visited[blueNearest.Id] == false && blueStore.Current < blueStore.MaxCap {
				blueStore.Current++
				totalCost += blueNearest.Distance
				visited[blueNearest.Id] = true
				blueResult = append(blueResult, blueNearest)
				heap.Push(&greenHeap, greenNearest)
				heap.Push(&redHeap, redNearest)
				counter++
			} else {
				heap.Push(&greenHeap, greenNearest)
				heap.Push(&redHeap, redNearest)
			}
		} else {
			if visited[greenNearest.Id] == false && greenStore.Current < greenStore.MaxCap {
				greenStore.Current++
				totalCost += greenNearest.Distance
				visited[greenNearest.Id] = true
				greenResult = append(greenResult, greenNearest)
				heap.Push(&redHeap, redNearest)
				heap.Push(&blueHeap, blueNearest)
				counter++
			} else {
				heap.Push(&redHeap, redNearest)
				heap.Push(&blueHeap, blueNearest)
			}
		}

	}
	// Logic
	fmt.Println("First cost (Without optimization)", totalCost)

	// Optimization
	for red_i, redDelivery := range redResult {
		for blue_i, blueDelivery := range blueResult {
			for green_i, greenDelivery := range greenResult {
				oldCost := distance(greenDelivery.Coordinate, greenStore.Coordinate) + distance(blueDelivery.Coordinate, blueStore.Coordinate)
				newCost := distance(greenDelivery.Coordinate, blueStore.Coordinate) + distance(blueDelivery.Coordinate, greenStore.Coordinate)

				oldCostGreen := distance(greenDelivery.Coordinate, greenStore.Coordinate) + distance(redDelivery.Coordinate, redStore.Coordinate)
				newCostGreen := distance(greenDelivery.Coordinate, redStore.Coordinate) + distance(redDelivery.Coordinate, greenStore.Coordinate)

				oldCostBlue := distance(redDelivery.Coordinate, redStore.Coordinate) + distance(blueDelivery.Coordinate, blueStore.Coordinate)
				newCostBlue := distance(redDelivery.Coordinate, blueStore.Coordinate) + distance(blueDelivery.Coordinate, redStore.Coordinate)

				diff_1 := oldCost - newCost
				diff_2 := oldCostGreen - newCostGreen
				diff_3 := oldCostBlue - newCostBlue
				if diff_1 >= 0 && diff_1 > diff_2 && diff_1 > diff_3 {
					greenResult[green_i], blueResult[blue_i] = blueResult[blue_i], greenResult[green_i]
					totalCost -= diff_1
					//totalCost += newCost
					greenDelivery = greenResult[green_i]
					blueDelivery = blueResult[blue_i]
				}
				if diff_2 >= 0 && diff_2 > diff_1 && diff_2 > diff_3 {
					greenResult[green_i], redResult[red_i] = redResult[red_i], greenResult[green_i]
					//totalCost -= oldCostRed
					totalCost -= diff_2
					greenDelivery = greenResult[green_i]
					redDelivery = redResult[red_i]
				}
				if diff_3 >= 0 && diff_3 > diff_1 && diff_3 > diff_2 {
					redResult[red_i], blueResult[blue_i] = blueResult[blue_i], redResult[red_i]
					totalCost -= diff_3
					//totalCost += newCost
					blueDelivery = blueResult[blue_i]
					redDelivery = redResult[red_i]
				}

			}
		}
	}

	fmt.Println("Final cost (With optimization) -- Total bird", totalCost)
	fmt.Println("#Red | #Blue | #Green")
	fmt.Println(redStore.Current, blueStore.Current, greenStore.Current)

	// Map drawing
	ctx := sm.NewContext()
	ctx.SetSize(2500, 2500)

	ctx.SetCenter(s2.LatLngFromDegrees((redStore.Coordinate.Lat+greenStore.Coordinate.Lat+blueStore.Coordinate.Lat)/3, (redStore.Coordinate.Lon+greenStore.Coordinate.Lon+blueStore.Coordinate.Lon)/3))
	ctx.AddMarker(sm.NewMarker(s2.LatLngFromDegrees(redStore.Coordinate.Lat, redStore.Coordinate.Lon), color.RGBA{0xff, 0, 0, 0xff}, 32.0))
	ctx.AddMarker(sm.NewMarker(s2.LatLngFromDegrees(blueStore.Coordinate.Lat, blueStore.Coordinate.Lon), color.RGBA{0, 0, 0xff, 0xff}, 32.0))
	ctx.AddMarker(sm.NewMarker(s2.LatLngFromDegrees(greenStore.Coordinate.Lat, greenStore.Coordinate.Lon), color.RGBA{0, 0xff, 0, 0xff}, 32.0))

	for _, delivery := range blueResult {
		marker := sm.NewMarker(s2.LatLngFromDegrees(delivery.Coordinate.Lat, delivery.Coordinate.Lon), color.RGBA{0, 0, 0xff, 0xff}, 16.0)
		marker.Label = fmt.Sprintf("%d", delivery.Id)
		ctx.AddMarker(marker)

	}
	for _, delivery := range greenResult {
		marker := sm.NewMarker(s2.LatLngFromDegrees(delivery.Coordinate.Lat, delivery.Coordinate.Lon), color.RGBA{0, 0xff, 0, 0xff}, 16.0)
		marker.Label = fmt.Sprintf("%d", delivery.Id)
		ctx.AddMarker(marker)

	}
	for _, delivery := range redResult {
		marker := sm.NewMarker(s2.LatLngFromDegrees(delivery.Coordinate.Lat, delivery.Coordinate.Lon), color.RGBA{0xff, 0, 0, 0xff}, 16.0)
		marker.Label = fmt.Sprintf("%d", delivery.Id)
		ctx.AddMarker(marker)
	}
	img, err := ctx.Render()
	if err != nil {
		panic(err)
	}

	if err := gg.SavePNG("assignments-of-deliveries.png", img); err != nil {
		panic(err)
	}
	// Map drawing

}
